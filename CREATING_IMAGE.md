# Creating and Uploading the Image

Write the initial settings to the card:
```
sudo ./setup_card.sh "your wifi ssid" "your wifi password"
```

Start up the Pi with this card. Run the setup script (initial password is `raspberry`):
```
ssh pi@hivemind
wget -O setup_image.sh 'https://gitlab.com/kqhivemind/hivemind-pi-config/-/raw/main/setup_image.sh?inline=false'
chmod +x setup_image.sh
sudo ./setup_image.sh
```

When the script finishes, the Pi will shut down. Take out the card and put it back in your PC.
```
export VERSION=1.00
export IMAGE="hivemind-pi-image-${VERSION}.img"
sudo dd if=/dev/mmcblk0 of="${IMAGE}" bs=4M conv=fsync status=progress

wget https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh
chmod +x pishrink.sh
sudo ./pishrink.sh "${IMAGE}"

zip "${IMAGE}.zip" "${IMAGE}"
s3cmd --configure
s3cmd put "${IMAGE}.zip" s3://kqhivemind/pi-images/
```
