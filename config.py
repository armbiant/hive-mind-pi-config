import json
import os
import stat
import subprocess

from flask import (Flask, jsonify, redirect, render_template, request,
                   send_from_directory)

app = Flask(__name__)

@app.route("/")
def index():
    if request.headers.get("Host") == "portal.kqhivemind.com":
        return render_template("index.html")
    else:
        return redirect("http://portal.kqhivemind.com")

@app.route("/favicon.ico")
def favicon():
    return send_from_directory("static", "favicon.ico")

@app.route("/configure", methods=["POST"])
def configure():
    data = request.json

    # Hostname
    hostname = "hivemind-{}-{}".format(data["cabinetName"], data["readerColor"])
    with open("/etc/hostname", "w") as fh:
        fh.write(hostname)

    subprocess.run([ "sed", "-i", "'s/^127.0.1.1\\b.*$/127.0.1.1       {}/'".format(hostname), "/etc/hosts" ])

    # Wifi
    os.remove("/etc/wpa_supplicant/wpa_supplicant.conf")
    os.remove("/etc/default/dnsmasq")
    subprocess.call([ "/usr/bin/systemctl", "disable", "dnsmasq" ])

    if data["enableWifi"]:
        output = subprocess.run([ "/usr/bin/wpa_passphrase", data["ssid"], data["psk"] ], capture_output=True)
        with open("/etc/wpa_supplicant/wpa_supplicant.conf", "w") as fh:
            fh.write("""
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=us

""")
            fh.write(output.stdout.decode("utf-8"))

        subprocess.call([ "/usr/bin/systemctl", "enable", "wpa_supplicant" ])

    # Router
    subprocess.call([ "firewall-cmd", "--zone=public", "--remove-service=dns" ])
    subprocess.call([ "firewall-cmd", "--zone=public", "--remove-service=dhcp" ])
    subprocess.call([ "firewall-cmd", "--zone=public", "--remove-service=http" ])

    if data["enableRouter"]:
        with open("/etc/dhcpcd.conf", "w") as fh:
            fh.write("""
hostname
clientid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option interface_mtu
require dhcp_server_identifier
slaac private

interface eth0
static ip_address=10.68.182.1
static domain_name_servers=8.8.8.8
""")

        with open("/etc/dhcp/dhcpd.conf", "w") as fh:
            fh.write("""
ddns-update-style none;
option domain-name "hivemind.local";
option domain-name-servers 8.8.8.8, 8.8.4.4, 10.68.182.1;
default-lease-time 3600;
max-lease-time 86400;
authoritative;
log-facility local7;

subnet 10.68.182.0 netmask 255.255.255.0 {
    range 10.68.182.100 10.68.182.100;
    option routers 10.68.182.1;
}
""")

        with open("/etc/default/isc-dhcp-server", "w") as fh:
            fh.write("""
INTERFACESv4="eth0"
INTERFACESv6=""
""")

        subprocess.call([ "/usr/bin/firewall-cmd", "--zone=public", "--add-masquerade" ])
        subprocess.call([ "/usr/bin/firewall-cmd", "--zone=home", "--add-service=dns" ])
        subprocess.call([ "/usr/bin/firewall-cmd", "--zone=home", "--add-service=dhcp" ])

        subprocess.call([ "/usr/bin/systemctl", "enable", "isc-dhcp-server" ])

    else:
        subprocess.call([ "/usr/bin/systemctl", "disable", "isc-dhcp-server" ])
        with open("/etc/dhcpcd.conf", "w") as fh:
            fh.write("""
hostname
clientid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option interface_mtu
require dhcp_server_identifier
slaac private
""")

    subprocess.call([ "/usr/bin/firewall-cmd", "--runtime-to-permanent" ])


    # Stats Client
    if data["enableStats"]:
        stats_config = {
            "cabinets": [
                {
                    "sceneName": data["sceneName"],
                    "cabinetName": data["cabinetName"],
                    "token": data["token"],
                    "url": "ws://10.68.182.100:12749",
                },
            ],
            "servers": [
                {
                    "name": "HiveMind",
                    "url": "wss://kqhivemind.com/ws/stats_listener/v3",
                },
            ],
        }

        with open("/home/hivemind/config.json", "w") as fh:
            fh.write(json.dumps(stats_config, sort_keys=True, indent=4))

        os.chown("/home/hivemind/config.json", 1001, 1001)

        subprocess.call([ "/usr/bin/systemctl", "enable", "hivemind-client" ])

    else:
        subprocess.call([ "/usr/bin/systemctl", "disable", "hivemind-client" ])

    # NFC Reader
    if data["enableNFC"]:
        nfc_config = {
            "pin_config": [
                { "player_id": "4" if data["readerColor"] == "blue" else "3", "button": 22, "light": 21 },
                { "player_id": "6" if data["readerColor"] == "blue" else "5", "button": 12, "light": 16 },
                { "player_id": "2" if data["readerColor"] == "blue" else "1", "button": 19, "light": 18 },
                { "player_id": "8" if data["readerColor"] == "blue" else "7", "button": 13, "light": 15 },
                { "player_id": "10" if data["readerColor"] == "blue" else "9", "button": 24, "light": 23 },
            ],
            "scene": data["sceneName"],
            "cabinet": data["cabinetName"],
            "token": data["token"],
            "reader": data["readerColor"],
            "usb_device": "usb:072f:2200",
            "log_file": "/home/hivemind/nfc-reader.log",
            "light_mode": "low",
            "button_mode": "low",
            "pins_low": [26],
        }

        with open("/home/hivemind/nfc-config.json", "w") as fh:
            fh.write(json.dumps(nfc_config, sort_keys=True, indent=4))

        os.chown("/home/hivemind/nfc-config.json", 1001, 1001)

        subprocess.call([ "/usr/bin/systemctl", "enable", "hivemind-nfc-reader" ])

    else:
        subprocess.call([ "/usr/bin/systemctl", "disable", "hivemind-nfc-reader" ])

    # SSH
    if data["enableSSH"]:
        os.mkdir("/home/hivemind/.ssh")
        os.chown("/home/hivemind/.ssh", 1001, 1001)
        os.chmod("/home/hivemind/.ssh", stat.S_IRWXU)

        with open("/home/hivemind/.ssh/authorized_keys", "w") as fh:
            fh.write(data["publicKeys"])

        os.chown("/home/hivemind/.ssh/authorized_keys", 1001, 1001)
        os.chmod("/home/hivemind/.ssh/authorized_keys", stat.S_IRUSR | stat.S_IWUSR)

        subprocess.call([ "/usr/bin/systemctl", "enable", "sshd" ])

    else:
        subprocess.call([ "/usr/bin/systemctl", "disable", "sshd" ])

    if data["password"]:
        subprocess.call([ "/usr/sbin/usermod", "hivemind", "-p", data["password"] ])
    else:
        subprocess.call([ "/usr/bin/passwd", "-l", "hivemind" ])

    # Disable config service and restart
    subprocess.call([ "/usr/bin/systemctl", "disable", "nodogsplash" ])
    subprocess.call([ "/usr/bin/systemctl", "disable", "hivemind-pi-config" ])
    subprocess.call([ "/usr/sbin/reboot" ])

    return jsonify({ "success": True })
