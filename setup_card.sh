#!/bin/bash
# Writes the initial Raspberry Pi OS image.
set -e

wifi_ssid="$1"
wifi_psk="$2"
sd_device="$3"

hostname="hivemind"

while [ -z "$sd_device" ]; do
    read -p "SD card device [/dev/mmcblk0]: " sd_device

    if [ -z "$sd_device" ]; then
        sd_device="/dev/mmcblk0"
    fi

    set +e
    sd_model=$( fdisk -l /dev/mmcblk0 | head -1 )
    if [ $? -ne 0 ]; then
        echo "Cannot use ${sd_device}. Try one of the following:"
        lsblk -np -o TYPE,NAME,MODEL,SIZE | grep "^disk" | cut -d' ' -f 2-
        echo

        sd_device=""
    fi
    set -e
done


# Write to SD card
mount | grep "^${sd_device}" | cut -d ' ' -f 3 | xargs -r umount
dd if=2021-05-07-raspios-buster-armhf-lite.img of="${sd_device}" bs=4M conv=fsync status=progress
sync


# Mount SD card
tempdir=$( mktemp -d )
rootfs="${tempdir}/rootfs"
boot="${tempdir}/boot"

mkdir -p "$boot" "$rootfs"

mount -t vfat "${sd_device}p1" "$boot"
mount -t ext4 "${sd_device}p2" "$rootfs"


# Enable SSH
touch "${boot}/ssh"


# Set hostname to hivemind
hosts_file=$( mktemp )
grep -v "raspberrypi" "${rootfs}/etc/hosts" > "$hosts_file"
echo "127.0.1.1       $hostname" >> "$hosts_file"
mv "$hosts_file" "${rootfs}/etc/hosts"


# Add wifi info
mkdir -p "${rootfs}/etc/wpa_supplicant"
cat > "${rootfs}/etc/wpa_supplicant/wpa_supplicant.conf" <<EOF
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=us

EOF

wpa_passphrase "${wifi_ssid}" "${wifi_psk}" | sudo tee -a "${rootfs}/etc/wpa_supplicant/wpa_supplicant.conf"


# Cleanup
sync
umount "$rootfs"
umount "$boot"
rm -rf "$tempdir"
