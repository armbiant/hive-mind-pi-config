#!/bin/bash
# Generates HiveMind image starting from Raspberry Pi OS base.
set -e


# Install packages
apt-get update
apt-get -o Dpkg::Options::='--force-confold' -y install git isc-dhcp-server firewalld \
        python3-pip python3-venv python3-flask libnfc-dev gunicorn3 hostapd dnsmasq

# Add HiveMind user
useradd -m -U -G sudo,gpio,plugdev  -p "$( openssl passwd --crypt HiveMind123 )" hivemind
usermod -L pi

cat /etc/sudoers | sed 's/^\%sudo.*$/%sudo  ALL=(ALL) NOPASSWD:ALL/' > /tmp/sudoers
chown root:root /tmp/sudoers
chmod 600 /tmp/sudoers
mv /tmp/sudoers /etc/sudoers

cd /home/hivemind


# Install Node.js
wget https://nodejs.org/dist/v11.15.0/node-v11.15.0-linux-armv6l.tar.gz
tar xfz node-v11.15.0-linux-armv6l.tar.gz
mv node-v11.15.0-linux-armv6l /usr/local/node
export PATH=$PATH:/usr/local/node/bin


# Install HiveMind client and startup script
cat > /home/hivemind/hivemind-client.sh <<EOF
#!/bin/bash
export PATH=$PATH:/usr/local/node/bin
cd /home/hivemind

if [ -e /boot/config.json ]; then
   cp /boot/config.json /home/hivemind/config.json
fi

npm upgrade @kqhivemind/hivemind-client
npx hivemind-client config.json >> /home/hivemind/hivemind-client.log
EOF

chmod +x /home/hivemind/hivemind-client.sh

cat > /lib/systemd/system/hivemind-client.service <<EOF
[Unit]
Description=HiveMind Client Service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/home/hivemind/hivemind-client.sh
User=hivemind

[Install]
WantedBy=multi-user.target
EOF


# Install NFC client and scripts
pip3 install hivemind_nfc_reader

cat > /home/hivemind/hivemind-nfc-reader.sh <<EOF
#!/bin/bash
if [ -e /boot/nfc-config.json ]; then
   cp /boot/nfc-config.json /home/hivemind/nfc-config.json
fi

pip3 install --upgrade hivemind-nfc-reader
/home/hivemind/.local/bin/hivemind-nfc-reader /home/hivemind/nfc-config.json
EOF

chmod +x /home/hivemind/hivemind-nfc-reader.sh

cat > /lib/systemd/system/hivemind-nfc-reader.service <<EOF
[Unit]
Description=HiveMind NFC Reader Service
After=network-online.target nss-lookup.target
Wants=network-online.target nss-lookup.target

[Service]
ExecStart=/home/hivemind/hivemind-nfc-reader.sh
User=hivemind

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable hivemind-nfc-reader


# Install config app
git clone https://gitlab.com/kqhivemind/hivemind-pi-config.git

cat > /lib/systemd/system/hivemind-pi-config.service <<EOF
[Unit]
Description=HiveMind Config Service
After=network-online.target nss-lookup.target
Wants=network-online.target nss-lookup.target

[Service]
ExecStart=/usr/bin/gunicorn3 -w 4 config:app -b 0.0.0.0:80 \
    --timeout 600 --error-logfile /home/hivemind/hivemind-pi-config.log
WorkingDirectory=/home/hivemind/hivemind-pi-config

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable hivemind-pi-config


# Set up captive portal
cat > /etc/dhcpcd.conf <<EOF
hostname
clientid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option interface_mtu
require dhcp_server_identifier
slaac private

env wpa_supplicant_conf=/etc/wpa_supplicant/wpa_supplicant.conf

interface wlan0
static ip_address=10.68.103.1
static domain_name_servers=10.68.103.1
EOF

firewall-cmd --zone=home --add-interface=eth0
firewall-cmd --zone=public --add-interface=wlan0
firewall-cmd --zone=public --add-service=dns
firewall-cmd --zone=public --add-service=dhcp
firewall-cmd --zone=public --add-service=http
firewall-cmd --runtime-to-permanent

cat > /etc/dnsmasq.conf <<EOF
listen-address=10.68.103.1
no-hosts
log-queries
log-facility=/var/log/dnsmasq.log
dhcp-range=10.68.103.2,10.68.103.254,72h
dhcp-option=option:router,10.68.103.1
dhcp-authoritative
dhcp-option=114,http://portal.kqhivemind.com
address=/#/10.68.103.1

EOF

cat > /etc/default/dnsmasq <<EOF
ENABLED=1
DNSMASQ_EXCEPT=lo
EOF

systemctl enable dnsmasq

cat > /etc/wpa_supplicant/wpa_supplicant.conf <<EOF
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
    ssid="HiveMind Config"
    mode=2
    key_mgmt=NONE
    frequency=2412
}
EOF

systemctl disable wpa_supplicant


# Cleanup
chown -R hivemind:hivemind /home/hivemind
rm -f /home/pi/.bash_history
rm -f /home/pi/setup_image.sh
systemctl enable regenerate_ssh_host_keys.service
history -c
shutdown now
